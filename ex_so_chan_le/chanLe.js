function result3() {


    // lấy biến dữ liệu
	var num1 = document.getElementById("txt-num1").value * 1;
	var num2 = document.getElementById("txt-num2").value * 1;
	var num3 = document.getElementById("txt-num3").value * 1;
	
    
    // Kiểm tra số nguyên.
	if (Math.ceil(num1) !== Math.floor(num1)) {
		return alert('Nhập dữ liệu là số nguyên Blyat.');
	}

	if (Math.ceil(num2) !== Math.floor(num2)) {
		return alert('Nhập dữ liệu là số nguyên Blyat.');
	}

	if (Math.ceil(num3) !== Math.floor(num3)) {
		return alert('Nhập dữ liệu là số nguyên Blyat.');
	}

    // Kiểm tra số Chẵn - Lẻ
	// Chẵn = 0, Lẻ = 1;
    
	var a = num1 % 2 === 0 ? 0 : 1;
	var b = num2 % 2 === 0 ? 0 : 1;
	var c = num3 % 2 === 0 ? 0 : 1;

	// Đếm số chẵn, lẻ
    // Lấy biến chẵn lẻ 3 số
	var sum = a + b + c;

    function soChanLe() {
		switch (sum) {
			case 0:
				return `Có 3 số chẵn, 0 số lẻ.`;
			case 1:
				return `Có 2 số chẵn, 1 số lẻ.`;
			case 2:
				return `Có 1 số chẵn, 2 số lẻ.`;
			case 3:
				return `Có 0 số chẵn, 3 số lẻ.`;
		}
	}
	var soChanLe = soChanLe();

    document.getElementById("result3").innerHTML = soChanLe;
}
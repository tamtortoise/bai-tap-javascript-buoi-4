function sapXep() {

 var a = document.getElementById("txt-so-nguyen-1").value*1;
 console.log('a: ', a);
    
 var b = document.getElementById("txt-so-nguyen-2").value*1;
 console.log('b: ', b);
    
 var c = document.getElementById("txt-so-nguyen-3").value*1;
 console.log('c: ', c);

 function sort() {

    // Các trường hợp từng số bé nhất:
    //  - a bé nhất:
    if (a < b && a < c) {
        if (b < c) {
            return `${a}, ${b}, ${c}`;
        } else if (b > c) {
            return `${a}, ${c}, ${b}`;
        } else {
            return `${a}, ${b}, ${c}`;
        }
    }

    //  - b bé nhất:
    else if (b < a && b < c) {
        if (a < c) {
            return `${b}, ${a}, ${c}`;
        } else if (a > c) {
            return `${b}, ${c}, ${a}`;
        } else {
            return `${b}, ${a}, ${c}`;
        }
    }

    // - c bé nhất:
    else if (c < a && c < b) {
        if (b < a) {
            return `${c}, ${b}, ${a}`;
        } else if (b > a) {
            return `${c}, ${a}, ${b}`;
        } else {
            return `${c}, ${b}, ${a}`;
        }
    }

    // Trường hợp từng số lớn nhất.
    else {
        if (a > b && a > c) {
            return `${c}, ${b}, ${a}`;
        } else if (b > a && b > c) {
            return `${c}, ${a}, ${b}`;
        } else if (c > a && c > b) {
            return `${a}, ${a}, ${c}`;
        }

        // Trường hợp 3 số bằng nhau:
        else {
            return `${a}, ${a}, ${c}`;
        }
    }
}

var sort = sort ();
console.log('sort: ', sort);
document.getElementById("result1").innerHTML = sort;
}